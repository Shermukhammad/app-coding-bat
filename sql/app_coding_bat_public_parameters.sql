INSERT INTO public.parameters (id, name, type) VALUES (1, 'strings', 'List<String>');
INSERT INTO public.parameters (id, name, type) VALUES (2, 'str', 'str');
INSERT INTO public.parameters (id, name, type) VALUES (3, 'nums', 'Integer[]');
INSERT INTO public.parameters (id, name, type) VALUES (4, 'a', 'int');
INSERT INTO public.parameters (id, name, type) VALUES (5, 'b', 'int');
INSERT INTO public.parameters (id, name, type) VALUES (6, 'c', 'int');