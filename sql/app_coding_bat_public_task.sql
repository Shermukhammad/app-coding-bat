INSERT INTO public.task (id, has_star, hint, method_return_type, name, solution, text, category_id) VALUES (1, false, null, 'List<String>', 'moreY', null, 'Given a list of strings, return a list where each string has "y" added at its start and end.', 4);
INSERT INTO public.task (id, has_star, hint, method_return_type, name, solution, text, category_id) VALUES (2, true, null, 'str', 'without_end', null, 'Given a string, return a version without the first and last char, so "Hello" yields "ell". The string length will be at least 2.', 2);
INSERT INTO public.task (id, has_star, hint, method_return_type, name, solution, text, category_id) VALUES (3, false, null, 'Boolean', 'noTriples', 'public Boolean noTriples(Integer[] nums) {
  // Iterate < length-2, so can use i+1 and i+2 in the loop.
   // Return false immediately if every seeing a triple.
 for (int i=0; i < (nums.length-2); i++) {
 int first = nums[i];
 if (nums[i+1]==first && nums[i+2]==first) return false;
 }
 // If we get here ... no triples
 return true;
 }', 'Given an array of ints, we''ll say that a triple is a value appearing 3 times in a row in the array. Return true if the array does not contain any triples.', 3);
INSERT INTO public.task (id, has_star, hint, method_return_type, name, solution, text, category_id) VALUES (4, false, null, 'bool', 'common_end', null, 'Given 2 arrays of ints, a and b, return True if they have the same first element or they have the same last element. Both arrays will be length 1 or more.', 5);
INSERT INTO public.task (id, has_star, hint, method_return_type, name, solution, text, category_id) VALUES (6, true, null, 'Integer[]', 'makeMiddle', null, 'Given an array of ints of even length, return a new array length 2 containing the middle two elements from the original array. The original array will be length 2 or more.', 6);
INSERT INTO public.task (id, has_star, hint, method_return_type, name, solution, text, category_id) VALUES (5, false, null, 'boolean', 'hasTeen', 'public boolean hasTeen(int a, int b, int c) {
 // Here it is written as one big expression,
 // vs. a series of if-statements.
 return (a>=13 && a<=19) || 
 (b>=13 && b<=19) || 
 (c>=13 && c<=19);
 }', 'We''ll say that a number is "teen" if it is in the range 13..19 inclusive. Given 3 int values, return true if 1 or more of them are teen.', 1);