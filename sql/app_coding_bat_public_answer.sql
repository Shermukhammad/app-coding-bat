INSERT INTO public.answer (id, is_correct, text, task_id, user_id) VALUES (1, true, 'return (a>=13 && a<=19 || b>=13 && b<=19 || c>=13 && c<=19);', 5, 3);
INSERT INTO public.answer (id, is_correct, text, task_id, user_id) VALUES (2, false, 'return str[1 : len(str)-1]', 2, 1);
INSERT INTO public.answer (id, is_correct, text, task_id, user_id) VALUES (3, false, 'return str[1 : len(str)-1]', 6, 4);
INSERT INTO public.answer (id, is_correct, text, task_id, user_id) VALUES (4, true, 'strings.replaceAll(s -> "y".concat(s.concat("y"))); 
return strings;', 1, 2);
INSERT INTO public.answer (id, is_correct, text, task_id, user_id) VALUES (5, true, 'bool(a[0] == b[0] or a[len(a)-1] == b[len(b)-1])', 4, 3);
INSERT INTO public.answer (id, is_correct, text, task_id, user_id) VALUES (6, false, 'for(int i = 0; i < nums.length-2; i++)
 if(nums[i] == nums[i+1] && nums[i] == nums[i+2])
 return false;
 return true;', 3, 2);