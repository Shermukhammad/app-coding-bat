package uz.pdp.appcodingbat.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.payload.CategoryAddDto;
import uz.pdp.appcodingbat.payload.CategoryDto;
import uz.pdp.appcodingbat.service.CategoryService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<CategoryDto> getCategoryById(@PathVariable
                                               @NotBlank(message = "Id should not be null")
                                               @Min(value = 1, message = "Id has to be positive integer")
                                                       Long id){
        return categoryService.getCategoryById(id);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<CategoryDto>> getCategoriesList(){
        return categoryService.getCategoriesList();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addCategory(@Valid @RequestBody CategoryAddDto categoryAddDto){
        return categoryService.addCategory(categoryAddDto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable
                                        @NotBlank(message = "Id should not be null")
                                        @Min(value = 1, message = "Id has to be positive integer")
                                                Long id,
                                        @Valid @RequestBody CategoryAddDto categoryAddDto){
        return categoryService.updateCategory(id, categoryAddDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable
                                        @NotBlank(message = "Id should not be null")
                                        @Min(value = 1, message = "Id has to be positive integer")
                                                Long id){
        return categoryService.deleteCategory(id);
    }

}
