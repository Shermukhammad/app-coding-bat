package uz.pdp.appcodingbat.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.payload.LanguageAddDto;
import uz.pdp.appcodingbat.payload.LanguageDto;
import uz.pdp.appcodingbat.service.LanguageService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/api/language")
public class LanguageController {

    private final LanguageService languageService;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<LanguageDto> getLanguageById(@PathVariable
                                                           @NotBlank(message = "Id should not be null")
                                                           @Min(value = 1, message = "Id has to be positive integer")
                                                                   Long id){
        return languageService.getLanguageById(id);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<LanguageDto>> getLanguagesList(){
        return languageService.getLanguagesList();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addLanguage(@Valid @RequestBody LanguageAddDto languageAddDto){
        return languageService.addLanguage(languageAddDto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateLanguage(@PathVariable
                                              @NotBlank(message = "Id should not be null")
                                              @Min(value = 1, message = "Id has to be positive integer")
                                                      Long id,
                                              @Valid @RequestBody LanguageAddDto languageAddDto){
        return languageService.updateLanguage(id, languageAddDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteLanguage(@PathVariable
                                              @NotBlank(message = "Id should not be null")
                                              @Min(value = 1, message = "Id has to be positive integer")
                                                      Long id){
        return languageService.deleteLanguage(id);
    }

}
