package uz.pdp.appcodingbat.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.payload.TaskAddDto;
import uz.pdp.appcodingbat.payload.TaskDto;
import uz.pdp.appcodingbat.service.TaskService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable
                                               @NotBlank(message = "Id should not be null")
                                               @Min(value = 1, message = "Id has to be positive integer")
                                                       Long id){
        return taskService.getTaskById(id);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<TaskDto>> getTasksList(){
        return taskService.getTasksList();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addTask(@Valid @RequestBody TaskAddDto taskAddDto){
        return taskService.addTask(taskAddDto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateTask(@PathVariable
                                        @NotBlank(message = "Id should not be null")
                                        @Min(value = 1, message = "Id has to be positive integer")
                                                Long id,
                                        @Valid @RequestBody TaskAddDto taskAddDto){
        return taskService.updateTask(id, taskAddDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable
                                        @NotBlank(message = "Id should not be null")
                                        @Min(value = 1, message = "Id has to be positive integer")
                                                Long id){
        return taskService.deleteTask(id);
    }

}
