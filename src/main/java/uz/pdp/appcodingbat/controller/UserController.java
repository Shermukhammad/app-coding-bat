package uz.pdp.appcodingbat.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.payload.UserAddDto;
import uz.pdp.appcodingbat.payload.UserDto;
import uz.pdp.appcodingbat.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable
                                                       @NotBlank(message = "Id should not be null")
                                                       @Min(value = 1, message = "Id has to be positive integer")
                                                               Long id){
        return userService.getUserById(id);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<UserDto>> getUsersList(){
        return userService.getUsersList();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addUser(@Valid @RequestBody UserAddDto userAddDto){
        return userService.addUser(userAddDto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@PathVariable
                                            @NotBlank(message = "Id should not be null")
                                            @Min(value = 1, message = "Id has to be positive integer")
                                                    Long id,
                                            @Valid @RequestBody UserAddDto userAddDto){
        return userService.updateUser(id, userAddDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable
                                            @NotBlank(message = "Id should not be null")
                                            @Min(value = 1, message = "Id has to be positive integer")
                                                    Long id){
        return userService.deleteUser(id);
    }


}

