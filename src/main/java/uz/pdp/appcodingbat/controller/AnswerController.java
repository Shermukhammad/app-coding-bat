package uz.pdp.appcodingbat.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcodingbat.payload.AnswerAddDto;
import uz.pdp.appcodingbat.payload.AnswerDto;
import uz.pdp.appcodingbat.service.AnswerService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/api/answer")
public class AnswerController {

    private final AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<AnswerDto> getAnswerById(@PathVariable
                                               @NotBlank(message = "Id should not be null")
                                               @Min(value = 1, message = "Id has to be positive integer")
                                                       Long id){
        return answerService.getAnswerById(id);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<AnswerDto>> getAnswersList(){
        return answerService.getAnswersList();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addAnswer(@Valid @RequestBody AnswerAddDto answerAddDto){
        return answerService.addAnswer(answerAddDto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateAnswer(@PathVariable
                                        @NotBlank(message = "Id should not be null")
                                        @Min(value = 1, message = "Id has to be positive integer")
                                                Long id,
                                        @Valid @RequestBody AnswerAddDto answerAddDto){
        return answerService.updateAnswer(id, answerAddDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAnswer(@PathVariable
                                        @NotBlank(message = "Id should not be null")
                                        @Min(value = 1, message = "Id has to be positive integer")
                                                Long id){
        return answerService.deleteAnswer(id);
    }

}
