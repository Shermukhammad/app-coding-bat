package uz.pdp.appcodingbat.service;

import org.springframework.http.ResponseEntity;
import uz.pdp.appcodingbat.payload.LanguageAddDto;
import uz.pdp.appcodingbat.payload.LanguageDto;

import java.util.List;

public interface LanguageService {

    ResponseEntity<?> addLanguage(LanguageAddDto languageAddDto);

    ResponseEntity<LanguageDto> getLanguageById(Long id);

    ResponseEntity<List<LanguageDto>> getLanguagesList();

    ResponseEntity<?> updateLanguage(Long id, LanguageAddDto languageAddDto);

    ResponseEntity<?> deleteLanguage(Long id);

}
