package uz.pdp.appcodingbat.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Category;
import uz.pdp.appcodingbat.entity.Language;
import uz.pdp.appcodingbat.entity.Task;
import uz.pdp.appcodingbat.mapper.CategoryMapper;
import uz.pdp.appcodingbat.payload.CategoryAddDto;
import uz.pdp.appcodingbat.payload.CategoryDto;
import uz.pdp.appcodingbat.repository.*;
import uz.pdp.appcodingbat.service.CategoryService;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final LanguageRepo languageRepo;
    private final CategoryRepo categoryRepo;
    private final CategoryMapper categoryMapper;
    private final AnswerRepo answerRepo;
    private final ParameterRepo parameterRepo;
    private final TaskRepo taskRepo;

    public CategoryServiceImpl(LanguageRepo languageRepo, CategoryRepo categoryRepo, CategoryMapper categoryMapper, AnswerRepo answerRepo, TaskRepo taskRepo, ParameterRepo parameterRepo) {
        this.languageRepo = languageRepo;
        this.categoryRepo = categoryRepo;
        this.categoryMapper = categoryMapper;
        this.answerRepo = answerRepo;
        this.taskRepo = taskRepo;
        this.parameterRepo = parameterRepo;
    }


    @Override
    public ResponseEntity<?> addCategory(CategoryAddDto categoryAddDto) {

        ResponseEntity<?> responseEntity =
                validateCategory(categoryAddDto.getName(), categoryAddDto.getLanguageId());

        if (responseEntity.getStatusCode().is4xxClientError())
            return responseEntity;

        Category category = categoryMapper.fromCategoryAddDto(categoryAddDto);

        category.setLanguage((Language) responseEntity.getBody());

        category = categoryRepo.save(category);

        return new ResponseEntity<>(categoryMapper.fromCategory(category), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<CategoryDto> getCategoryById(Long id) {
        Optional<Category> optCategory = categoryRepo.findById(id);
        return optCategory.map(category -> ResponseEntity.ok(categoryMapper.fromCategory(category)))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<List<CategoryDto>> getCategoriesList() {
        return ResponseEntity.ok(categoryMapper.fromCategory(categoryRepo.findAll()));
    }


    @Override
    public ResponseEntity<?> updateCategory(Long id, CategoryAddDto categoryAddDto) {

        Optional<Category> optCategory = categoryRepo.findById(id);

        if (optCategory.isPresent()){

            Category category = optCategory.get();

            Long languageId = categoryAddDto.getLanguageId();
            String name = categoryAddDto.getName();

            if (!category.getLanguage().getId().equals(languageId)){

                ResponseEntity<?> responseEntity = validateCategory(name, languageId);
                if (responseEntity.getStatusCode().is4xxClientError())
                    return responseEntity;

                category.setLanguage((Language) responseEntity.getBody());
            }
            else if (!category.getName().equals(name)){
                if (categoryRepo.existsByNameAndLanguage_Id(name, languageId))
                    return new ResponseEntity<>("This category in given language is already existent",
                            HttpStatus.FORBIDDEN);

                category.setName(name);
            }

            category.setDescription(categoryAddDto.getDescription());

            category = categoryRepo.save(category);

            return ResponseEntity.accepted().body(categoryMapper.fromCategory(category));
        }

        return addCategory(categoryAddDto);
    }


    @Override
    public ResponseEntity<?> deleteCategory(Long id) {

        Optional<Category> optCategory = categoryRepo.findById(id);
        if (!optCategory.isPresent())
            return new ResponseEntity<>("Category with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        Category category = optCategory.get();

        CategoryServiceImpl.deleteCategory(category, taskRepo,
                            parameterRepo, answerRepo, categoryRepo);

        return ResponseEntity.ok(categoryMapper.fromCategory(category));
    }


    private ResponseEntity<?> validateCategory(String name, Long languageId){

        Optional<Language> optLanguage = languageRepo.findById(languageId);
        if (!optLanguage.isPresent())
            return new ResponseEntity<>("Language with id = " +
                    languageId + " not found", HttpStatus.NOT_FOUND);

        if (categoryRepo.existsByNameAndLanguage_Id(name, languageId))
            return new ResponseEntity<>("This category in given language is already existent",
                    HttpStatus.FORBIDDEN);

        return ResponseEntity.ok(optLanguage.get());
    }


    public static void deleteCategory(Category category, TaskRepo taskRepo,
                                      ParameterRepo parameterRepo, AnswerRepo answerRepo,
                                      CategoryRepo categoryRepo){

        List<Task> allByCategory = taskRepo.findAllByCategory(category);

        for (Task task : allByCategory)
            TaskServiceImpl.deleteTask(task, taskRepo, parameterRepo, answerRepo);

        categoryRepo.delete(category);

    }

}
