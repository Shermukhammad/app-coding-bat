package uz.pdp.appcodingbat.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Category;
import uz.pdp.appcodingbat.entity.Language;
import uz.pdp.appcodingbat.mapper.LanguageMapper;
import uz.pdp.appcodingbat.payload.LanguageAddDto;
import uz.pdp.appcodingbat.payload.LanguageDto;
import uz.pdp.appcodingbat.repository.*;
import uz.pdp.appcodingbat.service.LanguageService;

import java.util.List;
import java.util.Optional;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepo languageRepo;
    private final CategoryRepo categoryRepo;
    private final TaskRepo taskRepo;
    private final AnswerRepo answerRepo;
    private final ParameterRepo parameterRepo;
    private final LanguageMapper languageMapper;

    public LanguageServiceImpl(LanguageRepo languageRepo, CategoryRepo categoryRepo,
                               TaskRepo taskRepo, AnswerRepo answerRepo, ParameterRepo parameterRepo,
                               LanguageMapper languageMapper) {
        this.languageRepo = languageRepo;
        this.categoryRepo = categoryRepo;
        this.taskRepo = taskRepo;
        this.answerRepo = answerRepo;
        this.parameterRepo = parameterRepo;
        this.languageMapper = languageMapper;
    }


    @Override
    public ResponseEntity<?> addLanguage(LanguageAddDto languageAddDto) {

        String name = languageAddDto.getName();

        if (languageRepo.existsByName(name))
            return new ResponseEntity<>("Given Language is already existent",
                    HttpStatus.FORBIDDEN);

        Language language = new Language();
        language.setName(name);

        language = languageRepo.save(language);

        return new ResponseEntity<>(languageMapper.fromLanguage(language),
                HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<LanguageDto> getLanguageById(Long id) {
        Optional<Language> byId = languageRepo.findById(id);
        return byId.map(language -> ResponseEntity.ok(languageMapper.fromLanguage(language)))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<List<LanguageDto>> getLanguagesList() {
        return ResponseEntity.ok(languageMapper.fromLanguage(languageRepo.findAll()));
    }


    @Override
    public ResponseEntity<?> updateLanguage(Long id, LanguageAddDto languageAddDto) {

        Optional<Language> byId = languageRepo.findById(id);

        if (byId.isPresent()){

            Language language = byId.get();

            String name = languageAddDto.getName();

            if (!language.getName().equals(name)) {

                if (languageRepo.existsByName(name))
                    return new ResponseEntity<>("Given Language is already existent",
                            HttpStatus.FORBIDDEN);

                language.setName(name);
                language = languageRepo.save(language);
            }

            return ResponseEntity.accepted().body(languageMapper.fromLanguage(language));
        }

        return addLanguage(languageAddDto);
    }


    @Override
    public ResponseEntity<?> deleteLanguage(Long id) {

        Optional<Language> byId = languageRepo.findById(id);
        if (!byId.isPresent())
            return new ResponseEntity<>("Language with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        Language language = byId.get();

        for (Category category : categoryRepo.findAllByLanguage_Id(id))
            CategoryServiceImpl.deleteCategory(category, taskRepo, parameterRepo,
                                    answerRepo, categoryRepo);

        languageRepo.delete(language);

        return ResponseEntity.ok(languageMapper.fromLanguage(language));
    }



}
