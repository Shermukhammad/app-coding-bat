package uz.pdp.appcodingbat.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.User;
import uz.pdp.appcodingbat.mapper.UserMapper;
import uz.pdp.appcodingbat.payload.UserAddDto;
import uz.pdp.appcodingbat.payload.UserDto;
import uz.pdp.appcodingbat.repository.AnswerRepo;
import uz.pdp.appcodingbat.repository.UserRepo;
import uz.pdp.appcodingbat.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final UserMapper userMapper;
    private final AnswerRepo answerRepo;

    public UserServiceImpl(UserRepo userRepo, UserMapper userMapper, AnswerRepo answerRepo) {
        this.userRepo = userRepo;
        this.userMapper = userMapper;
        this.answerRepo = answerRepo;
    }


    @Override
    public ResponseEntity<?> addUser(UserAddDto userAddDto) {

        String email = userAddDto.getEmail();

        if (userRepo.existsByEmail(email))
            return new ResponseEntity<>("User with given email is already existent",
                    HttpStatus.FORBIDDEN);

        User user = userMapper.fromUserAddDto(userAddDto);

        user = userRepo.save(user);

        return new ResponseEntity<>(userMapper.fromUser(user), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<UserDto> getUserById(Long id) {
        Optional<User> byId = userRepo.findById(id);
        return byId.map(user -> ResponseEntity.ok(userMapper.fromUser(user)))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<List<UserDto>> getUsersList() {
        return ResponseEntity.ok(userMapper.fromUser(userRepo.findAll()));
    }


    @Override
    public ResponseEntity<?> updateUser(Long id, UserAddDto userAddDto) {

        Optional<User> byId = userRepo.findById(id);

        if (byId.isPresent()){

            User user = byId.get();

            String email = userAddDto.getEmail();

            if (!user.getEmail().equals(email)){
                if (userRepo.existsByEmail(email))
                    return new ResponseEntity<>("User with given email is already existent",
                            HttpStatus.FORBIDDEN);
                user.setEmail(email);
            }

            user.setPassword(userAddDto.getPassword());

            user = userRepo.save(user);

            return ResponseEntity.accepted().body(userMapper.fromUser(user));
        }

        return addUser(userAddDto);
    }

    @Override
    public ResponseEntity<?> deleteUser(Long id) {

        Optional<User> byId = userRepo.findById(id);
        if (!byId.isPresent())
            return new ResponseEntity<>("User with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        User user = byId.get();

        answerRepo.deleteAllByUser_Id(id);

        userRepo.delete(user);

        return ResponseEntity.ok(userMapper.fromUser(user));
    }

}