package uz.pdp.appcodingbat.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.entity.Task;
import uz.pdp.appcodingbat.entity.User;
import uz.pdp.appcodingbat.mapper.AnswerMapper;
import uz.pdp.appcodingbat.payload.AnswerAddDto;
import uz.pdp.appcodingbat.payload.AnswerDto;
import uz.pdp.appcodingbat.repository.AnswerRepo;
import uz.pdp.appcodingbat.repository.TaskRepo;
import uz.pdp.appcodingbat.repository.UserRepo;
import uz.pdp.appcodingbat.service.AnswerService;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final TaskRepo taskRepo;
    private final UserRepo userRepo;
    private final AnswerRepo answerRepo;
    private final AnswerMapper answerMapper;

    public AnswerServiceImpl(TaskRepo taskRepo, UserRepo userRepo, AnswerRepo answerRepo, AnswerMapper answerMapper) {
        this.taskRepo = taskRepo;
        this.userRepo = userRepo;
        this.answerRepo = answerRepo;
        this.answerMapper = answerMapper;
    }


    @Override
    public ResponseEntity<?> addAnswer(AnswerAddDto answerAddDto) {

        Long taskId = answerAddDto.getTaskId();
        Optional<Task> optTask = taskRepo.findById(taskId);
        if (!optTask.isPresent())
            return new ResponseEntity<>("Task with id = " + taskId
                    + " not found", HttpStatus.NOT_FOUND);

        Long userId = answerAddDto.getUserId();
        Optional<User> optUser = userRepo.findById(userId);
        if (!optUser.isPresent())
            return new ResponseEntity<>("User with id = " + userId
                    + " not found", HttpStatus.NOT_FOUND);

        Answer answer = answerMapper.fromAnswerAddDto(answerAddDto);
        answer.setTask(optTask.get());
        answer.setUser(optUser.get());

        answer = answerRepo.save(answer);

        return new ResponseEntity<>(answerMapper.fromAnswer(answer), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<AnswerDto> getAnswerById(Long id) {
        Optional<Answer> optAnswer = answerRepo.findById(id);
        return optAnswer.map(answer -> ResponseEntity.ok(answerMapper.fromAnswer(answer)))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<List<AnswerDto>> getAnswersList() {
        return ResponseEntity.ok(answerMapper.fromAnswer(answerRepo.findAll()));
    }


    @Override
    public ResponseEntity<?> updateAnswer(Long id, AnswerAddDto answerAddDto) {

        Optional<Answer> optAnswer = answerRepo.findById(id);

        if (optAnswer.isPresent()){

            Answer answer = optAnswer.get();

            Long taskId = answerAddDto.getTaskId();
            if (!answer.getTask().getId().equals(taskId)){

                Optional<Task> optTask = taskRepo.findById(taskId);
                if (!optTask.isPresent())
                    return new ResponseEntity<>("Task with id = " + taskId
                            + " not found", HttpStatus.NOT_FOUND);

                answer.setTask(optTask.get());
            }

            Long userId = answerAddDto.getUserId();
            if (!answer.getUser().getId().equals(userId)){

                Optional<User> optUser = userRepo.findById(userId);
                if (!optUser.isPresent())
                    return new ResponseEntity<>("User with id = " + userId
                            + " not found", HttpStatus.NOT_FOUND);

                answer.setUser(optUser.get());
            }

            answer.setText(answerAddDto.getText());
            answer.setIsCorrect(answerAddDto.getIsCorrect());

            answer = answerRepo.save(answer);

            return ResponseEntity.accepted().body(answerMapper.fromAnswer(answer));
        }


        return addAnswer(answerAddDto);
    }


    @Override
    public ResponseEntity<?> deleteAnswer(Long id) {

        Optional<Answer> optAnswer = answerRepo.findById(id);

        if (!optAnswer.isPresent())
            return new ResponseEntity<>("Answer with id = " + id
                    + " not found", HttpStatus.NOT_FOUND);

        Answer answer = optAnswer.get();

        answerRepo.delete(answer);

        return ResponseEntity.ok(answerMapper.fromAnswer(answer));
    }


}
