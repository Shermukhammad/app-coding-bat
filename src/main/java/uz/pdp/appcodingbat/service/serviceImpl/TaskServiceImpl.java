package uz.pdp.appcodingbat.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcodingbat.entity.Category;
import uz.pdp.appcodingbat.entity.Example;
import uz.pdp.appcodingbat.entity.Parameter;
import uz.pdp.appcodingbat.entity.Task;
import uz.pdp.appcodingbat.mapper.TaskMapper;
import uz.pdp.appcodingbat.payload.*;
import uz.pdp.appcodingbat.repository.*;
import uz.pdp.appcodingbat.service.TaskService;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
public class TaskServiceImpl implements TaskService {

    private final CategoryRepo categoryRepo;
    private final TaskRepo taskRepo;
    private final ParameterRepo parameterRepo;
    private final AnswerRepo answerRepo;
    private final TaskMapper taskMapper;

    public TaskServiceImpl(CategoryRepo categoryRepo, TaskRepo taskRepo,
                           ParameterRepo parameterRepo, AnswerRepo answerRepo,
                           TaskMapper taskMapper) {
        this.categoryRepo = categoryRepo;
        this.taskRepo = taskRepo;
        this.parameterRepo = parameterRepo;
        this.answerRepo = answerRepo;
        this.taskMapper = taskMapper;
    }

    @Override
    public ResponseEntity<?> addTask(TaskAddDto taskAddDto) {

        ResponseEntity<?> responseEntity = validateTask(taskAddDto.getName(), taskAddDto.getCategoryId());
        if (responseEntity.getStatusCode().is4xxClientError())
            return responseEntity;

        Task task = taskMapper.fromTaskAddDto(taskAddDto);
        task.setCategory((Category) responseEntity.getBody());

        Set<Example> examples = new HashSet<>();
        for (String exampleText : taskAddDto.getExampleTexts()) {
            Example example = new Example();
            example.setText(exampleText);
            examples.add(example);
        }
        task.setExamples(examples);

        task.setParameters(realizeParameters(taskAddDto.getParametersSet()));

        task = taskRepo.save(task);

        return new ResponseEntity<>(taskMapper.fromTask(task), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<TaskDto> getTaskById(Long id) {
        Optional<Task> byId = taskRepo.findById(id);
        return byId.map(task -> ResponseEntity.ok(taskMapper.fromTask(task)))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<List<TaskDto>> getTasksList() {
        return ResponseEntity.ok(taskMapper.fromTask(taskRepo.findAll()));
    }


    @Override
    public ResponseEntity<?> updateTask(Long id, TaskAddDto taskAddDto) {

        Optional<Task> optTask = taskRepo.findById(id);
        if (optTask.isPresent()){

            Task task = optTask.get();

            Long categoryId = taskAddDto.getCategoryId();
            String name = taskAddDto.getName();

            if (!categoryId.equals(task.getCategory().getId())){

                ResponseEntity<?> responseEntity = validateTask(name, categoryId);
                if (responseEntity.getStatusCode().is4xxClientError())
                    return responseEntity;

                task.setCategory((Category) responseEntity.getBody());
            }
            else if (!name.equals(task.getName())){
                if (taskRepo.existsByNameAndCategory_Id(name, categoryId))
                    return new ResponseEntity<>("This task in given category is already existent",
                            HttpStatus.FORBIDDEN);
            }

            task = taskMapper.fromTaskAndTaskAddDto(task, taskAddDto);

            Set<Example> examples = new HashSet<>();

            Set<Example> examplesOriginalTask = task.getExamples();

            for (String exampleText : taskAddDto.getExampleTexts()) {

                Optional<Example> example1 = examplesOriginalTask
                                                    .stream()
                                                    .filter(example -> example.getText().equals(exampleText))
                                                    .findAny();

                if (example1.isPresent())
                    examples.add(example1.get());
                else {
                    Example example = new Example();
                    example.setText(exampleText);
                    examples.add(example);
                }

            }

            task.setExamples(examples);

            task.setParameters(realizeParameters(taskAddDto.getParametersSet()));

            task = taskRepo.save(task);

            return ResponseEntity.accepted().body(taskMapper.fromTask(task));
        }

        return addTask(taskAddDto);
    }


    @Override
    public ResponseEntity<?> deleteTask(Long id) {

        Optional<Task> byId = taskRepo.findById(id);
        if (!byId.isPresent())
            return new ResponseEntity<>("Task with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        Task task = byId.get();

        TaskDto taskDto = taskMapper.fromTask(task);

        deleteTask(task, taskRepo, parameterRepo, answerRepo);

        return ResponseEntity.ok(taskDto);
    }


    private Set<Parameter> realizeParameters(Set<ParameterAddDto> parametersSet){

        Set<Parameter> parameters = new HashSet<>();

        for (ParameterAddDto parameterAddDto : parametersSet) {

            String type = parameterAddDto.getType();

            String parameterName = parameterAddDto.getName();

            Optional<Parameter> byTypeAndName = parameterRepo.findByTypeAndName(type, parameterName);
            if (byTypeAndName.isPresent())
                parameters.add(byTypeAndName.get());
            else {
                Parameter parameter = new Parameter();
                parameter.setType(type);
                parameter.setName(parameterName);
                parameters.add(parameterRepo.save(parameter));
            }
        }

        return parameters;
    }


    private ResponseEntity<?> validateTask(String name, Long categoryId){

        Optional<Category> byId = categoryRepo.findById(categoryId);
        if (!byId.isPresent())
            return new ResponseEntity<>("Category with id = " + categoryId + " not found",
                    HttpStatus.NOT_FOUND);

        if (taskRepo.existsByNameAndCategory_Id(name, categoryId))
            return new ResponseEntity<>("This task in given category is already existent",
                    HttpStatus.FORBIDDEN);

        return ResponseEntity.ok(byId.get());
    }


    public static void deleteTask(Task task, TaskRepo taskRepo,
                                  ParameterRepo parameterRepo, AnswerRepo answerRepo){

        Long id = task.getId();

        for (Parameter parameter : task.getParameters())
            if (!taskRepo.existsByIdNotAndParameterId(id, parameter.getId())) {
                task.getParameters().remove(parameter);
                parameterRepo.delete(parameter);
            }

        answerRepo.deleteAllByTask_Id(id);

        taskRepo.delete(task);

    }


}
