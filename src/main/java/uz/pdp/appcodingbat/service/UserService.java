package uz.pdp.appcodingbat.service;

import org.springframework.http.ResponseEntity;
import uz.pdp.appcodingbat.payload.UserAddDto;
import uz.pdp.appcodingbat.payload.UserDto;

import java.util.List;

public interface UserService {

    ResponseEntity<?> addUser(UserAddDto userAddDto);

    ResponseEntity<UserDto> getUserById(Long id);

    ResponseEntity<List<UserDto>> getUsersList();

    ResponseEntity<?> updateUser(Long id, UserAddDto userAddDto);

    ResponseEntity<?> deleteUser(Long id);

}
