package uz.pdp.appcodingbat.service;

import org.springframework.http.ResponseEntity;
import uz.pdp.appcodingbat.payload.AnswerAddDto;
import uz.pdp.appcodingbat.payload.AnswerDto;

import java.util.List;

public interface AnswerService {

    ResponseEntity<?> addAnswer(AnswerAddDto answerAddDto);

    ResponseEntity<AnswerDto> getAnswerById(Long id);

    ResponseEntity<List<AnswerDto>> getAnswersList();

    ResponseEntity<?> updateAnswer(Long id, AnswerAddDto answerAddDto);

    ResponseEntity<?> deleteAnswer(Long id);

}
