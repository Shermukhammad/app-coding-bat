package uz.pdp.appcodingbat.service;

import org.springframework.http.ResponseEntity;
import uz.pdp.appcodingbat.payload.TaskAddDto;
import uz.pdp.appcodingbat.payload.TaskDto;

import java.util.List;

public interface TaskService {

    ResponseEntity<?> addTask(TaskAddDto taskAddDto);

    ResponseEntity<TaskDto> getTaskById(Long id);

    ResponseEntity<List<TaskDto>> getTasksList();

    ResponseEntity<?> updateTask(Long id, TaskAddDto taskAddDto);

    ResponseEntity<?> deleteTask(Long id);

}
