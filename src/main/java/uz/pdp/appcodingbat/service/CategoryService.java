package uz.pdp.appcodingbat.service;

import org.springframework.http.ResponseEntity;
import uz.pdp.appcodingbat.payload.CategoryAddDto;
import uz.pdp.appcodingbat.payload.CategoryDto;

import java.util.List;

public interface CategoryService {

    ResponseEntity<?> addCategory(CategoryAddDto categoryAddDto);

    ResponseEntity<CategoryDto> getCategoryById(Long id);

    ResponseEntity<List<CategoryDto>> getCategoriesList();

    ResponseEntity<?> updateCategory(Long id, CategoryAddDto categoryAddDto);

    ResponseEntity<?> deleteCategory(Long id);

}
