package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryAddDto implements Serializable {

    @NotNull(message = "Category name not given")
    private String name;

    @NotNull(message = "Category description not given")
    private String description;

    @NotNull(message = "Category Language not given")
    private Long languageId;

}
