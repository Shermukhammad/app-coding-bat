package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TaskAddDto implements Serializable {

    @NotNull(message = "Task name not given")
    private String name;

    @NotNull(message = "Task text not given")
    private String text;

    private String solution=null;

    private String hint=null;

    @NotNull(message = "Method return type not given")
    private String methodReturnType;

    private Boolean hasStar;

    @NotNull(message = "Task example not given")
    private Set<String> exampleTexts;

    private Set<ParameterAddDto> parametersSet;

    @NotNull(message = "Task Category not given")
    private Long categoryId;

}
