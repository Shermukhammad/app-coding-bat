package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TaskDto implements Serializable {

    private Long id;

    private String name;

    private String text;

    private String solution;

    private String hint;

    private String methodReturnType;

    private Boolean hasStar;

    private Set<ExampleDto> examplesSet;

    private Set<ParameterDto> parametersSet;

    private String categoryName;

    private String languageName;

}
