package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserAddDto implements Serializable {

    @NotNull(message = "Email not given")
    private String email;

    @NotNull(message = "Password not given")
    private String password;

}
