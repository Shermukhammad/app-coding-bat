package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AnswerDto implements Serializable {

    private String text;

    private String userEmail;

    private String taskName;

    private String categoryName;

    private String languageName;

}
