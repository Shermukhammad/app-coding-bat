package uz.pdp.appcodingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AnswerAddDto implements Serializable {

    private String text="";

    @NotNull(message = "Answer Task id not given")
    private Long taskId;

    @NotNull(message = "Answer user not given")
    private Long userId;

    @NotNull(message = "It is not given whether answer is correct or not")
    private Boolean isCorrect;

}
