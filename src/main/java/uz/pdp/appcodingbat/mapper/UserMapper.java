package uz.pdp.appcodingbat.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcodingbat.entity.User;
import uz.pdp.appcodingbat.payload.UserAddDto;
import uz.pdp.appcodingbat.payload.UserDto;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    UserDto fromUser(User user);

    List<UserDto> fromUser(List<User> users);

    User fromUserAddDto(UserAddDto userAddDto);

}
