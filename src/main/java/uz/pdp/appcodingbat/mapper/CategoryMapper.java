package uz.pdp.appcodingbat.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcodingbat.entity.Category;
import uz.pdp.appcodingbat.payload.CategoryAddDto;
import uz.pdp.appcodingbat.payload.CategoryDto;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoryMapper {

    @Mapping(source = "language.name", target = "languageName")
    CategoryDto fromCategory(Category category);

    List<CategoryDto> fromCategory(List<Category> categories);

    Category fromCategoryAddDto(CategoryAddDto categoryAddDto);

}
