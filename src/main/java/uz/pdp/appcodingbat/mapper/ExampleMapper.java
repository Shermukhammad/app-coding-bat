package uz.pdp.appcodingbat.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcodingbat.entity.Example;
import uz.pdp.appcodingbat.payload.ExampleDto;

import java.util.Set;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ExampleMapper {

    ExampleDto fromExample(Example example);

    Set<ExampleDto> fromExample(Set<Example> examples);

}
