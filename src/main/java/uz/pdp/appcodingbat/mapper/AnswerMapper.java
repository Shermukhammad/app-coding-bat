package uz.pdp.appcodingbat.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcodingbat.entity.Answer;
import uz.pdp.appcodingbat.payload.AnswerAddDto;
import uz.pdp.appcodingbat.payload.AnswerDto;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AnswerMapper {

    @Mapping(source = "user.email", target = "userEmail")
    @Mapping(source = "task.name", target = "taskName")
    @Mapping(source = "task.category.name", target = "categoryName")
    @Mapping(source = "task.category.language.name", target = "languageName")
    AnswerDto fromAnswer(Answer source);

    List<AnswerDto> fromAnswer(List<Answer> answers);

    Answer fromAnswerAddDto(AnswerAddDto answerAddDto);

}
