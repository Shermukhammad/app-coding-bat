package uz.pdp.appcodingbat.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcodingbat.entity.Parameter;
import uz.pdp.appcodingbat.entity.Task;
import uz.pdp.appcodingbat.payload.ParameterDto;
import uz.pdp.appcodingbat.payload.TaskAddDto;
import uz.pdp.appcodingbat.payload.TaskDto;

import java.util.List;
import java.util.Set;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
    uses = ExampleMapper.class)
public interface TaskMapper {

    @Mapping(source = "examples", target = "examplesSet")
    @Mapping(source = "parameters", target = "parametersSet")
    @Mapping(source = "category.name", target = "categoryName")
    @Mapping(source = "category.language.name", target = "languageName")
    TaskDto fromTask(Task task);

    List<TaskDto> fromTask(List<Task> tasks);

    Task fromTaskAddDto(TaskAddDto taskAddDto);

    @Mapping(source = "task.id", target = "id")
    @Mapping(source = "taskAddDto.name", target = "name")
    @Mapping(source = "taskAddDto.text", target = "text")
    @Mapping(source = "taskAddDto.solution", target = "solution")
    @Mapping(source = "taskAddDto.hint", target = "hint")
    @Mapping(source = "taskAddDto.methodReturnType", target = "methodReturnType")
    @Mapping(source = "taskAddDto.hasStar", target = "hasStar")
    @Mapping(source = "task.examples", target = "examples")
    @Mapping(source = "task.parameters", target = "parameters")
    @Mapping(source = "task.category", target = "category")
    Task fromTaskAndTaskAddDto(Task task, TaskAddDto taskAddDto);

    ParameterDto fromParameter(Parameter parameter);

    Set<ParameterDto> fromParameter(Set<Parameter> parameters);

}
