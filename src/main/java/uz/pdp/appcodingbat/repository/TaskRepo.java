package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.appcodingbat.entity.Category;
import uz.pdp.appcodingbat.entity.Task;

import java.util.List;

public interface TaskRepo extends JpaRepository<Task, Long> {

    Boolean existsByNameAndCategory_Id(String name, Long categoryId);

    @Query(value = "select case when count(t) > 0" +
            " then true else false end " +
            " from Task t" +
            " join t.parameters p where t.id <> :taskId and p.id = :parId")
    Boolean existsByIdNotAndParameterId(@Param("taskId") Long taskId,
                                        @Param("parId") Long parId);


    List<Task> findAllByCategory(Category category);

}
