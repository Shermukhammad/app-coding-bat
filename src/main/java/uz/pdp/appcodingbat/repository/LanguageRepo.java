package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcodingbat.entity.Language;

public interface LanguageRepo extends JpaRepository<Language, Long> {

    Boolean existsByName(String name);

}
