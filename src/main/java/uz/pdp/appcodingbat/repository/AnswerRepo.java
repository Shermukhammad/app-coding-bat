package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcodingbat.entity.Answer;

public interface AnswerRepo extends JpaRepository<Answer, Long> {

    void deleteAllByTask_Id(Long taskId);

    void deleteAllByUser_Id(Long userId);

}
