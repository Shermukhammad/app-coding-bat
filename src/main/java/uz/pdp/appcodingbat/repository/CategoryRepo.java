package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcodingbat.entity.Category;

import java.util.List;

public interface CategoryRepo extends JpaRepository<Category, Long> {

    Boolean existsByNameAndLanguage_Id(String name, Long languageId);

    List<Category> findAllByLanguage_Id(Long languageId);

}
