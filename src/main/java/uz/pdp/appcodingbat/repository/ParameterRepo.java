package uz.pdp.appcodingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcodingbat.entity.Parameter;

import java.util.Optional;

public interface ParameterRepo extends JpaRepository<Parameter, Long> {

    Optional<Parameter> findByTypeAndName(String type, String name);

}
