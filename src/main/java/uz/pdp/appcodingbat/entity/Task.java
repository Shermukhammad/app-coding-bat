package uz.pdp.appcodingbat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String text;

    @Column(length = 512)
    private String solution;

    @Column
    private String hint;

    @Column(nullable = false)
    private String methodReturnType;

    @Column
    private Boolean hasStar=false;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Example> examples;

    @ManyToMany
    private Set<Parameter> parameters;

    @ManyToOne(optional = false)
    private Category category;

}
